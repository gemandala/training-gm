function ValidateLineItem(type){
	

//get current line item sold by group

var currentItemSoldByGroups = nlapiGetCurrentLineItemValue('item', 
														   'custitem_360_crr_soldbygroup');


//item is open and valid for everybody if sold by group is empty
if(!currentItemSoldByGroups || 
		currentItemSoldByGroups.length==0) return true;



//pull id of currently logged in user
var userId = nlapiGetUser();


//search for user and get their custentity_360_crr_comgroups value
var searchresults = nlapiSearchRecord('employee', 
									   userId , 
									   null, 
									   ["custentity_360_crr_comgroups"]);


if(searchresults && 
		searchresults[0]){
	//pulls sold by groups of current user
	var userComgroups = searchresults[0].getValue('custentity_360_crr_comgroups');

	if(userComgroups)
	{
		//see if there's a match in either of the lists
		for(var i=0;i<userComgroups.length;i++){

			var userComgroup = 	userComgroups[i]?userComgroups[i].toLowerCase():null;
			for(var k=0;k<currentItemSoldByGroups.length;k++){
				
				var currentItemSoldByGroup = 	currentItemSoldByGroups[k]?
												currentItemSoldByGroups[k].toLowerCase():
												null;
				
				//if there is a match then the record is validated
				if(currentItemSoldByGroup && 
						userComgroup && 
							currentItemSoldByGroup==userComgroup)
				return true;

			}


		}



	}
	
	//The item isn't open but the user does not have any groups so return false
	
}

return false;

}